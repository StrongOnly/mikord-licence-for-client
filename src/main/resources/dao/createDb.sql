CREATE DATABASE "licenceClients2"
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;


INSERT INTO clients (key, name, surname) VALUES
('100001', 'Vassily', 'Petrov'),
('100002', 'Petr', 'Vasiliev');