package ru.tihomirov.mikordlicence.domain.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;
import ru.tihomirov.mikordlicence.domain.entity.enums.ClientStatus;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "clients")
public class Client {
    @Id
    @Column
    private String key;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @Column(name = "first_request_date_time")
    private LocalDateTime firstRequestDateTime;

    @Column(name = "request_count", columnDefinition = "bigint default 0")
    private int requestCount;

    @Column(name = "client_status", columnDefinition = "varchar default 'ACTIVE'")
    @Enumerated(value = EnumType.STRING)
    private ClientStatus clientStatus;

    @Column(name = "code_after_decode")
    private Long codeAfterDecode;


}
