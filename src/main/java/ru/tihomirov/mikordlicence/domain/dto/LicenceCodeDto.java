package ru.tihomirov.mikordlicence.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@ApiModel
@AllArgsConstructor
@Getter
public class LicenceCodeDto {

    @ApiModelProperty(value = "Ключ лицензии")
    private String licenseCode;

    public static LicenceCodeDto of(String licenceCode) {
        return new LicenceCodeDto(licenceCode);
    }
}
