package ru.tihomirov.mikordlicence.controller;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tihomirov.mikordlicence.domain.dto.LicenceCodeDto;
import ru.tihomirov.mikordlicence.service.impl.ClientServiceImpl;


@Api
@RestController
@RequiredArgsConstructor
public class ClientController {

    private final ClientServiceImpl clientService;

    @GetMapping("/client")
    public LicenceCodeDto greeting(@RequestParam(name = "key") String clientKey,
                                   @RequestParam(name = "code") String code) {
        clientService.update(clientKey, code);
        return clientService.licenceCode(clientKey, code);
    }

}
