package ru.tihomirov.mikordlicence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import ru.tihomirov.mikordlicence.domain.entity.Client;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, String> {

    boolean existsByKey(String key);

    Optional<Client> findByKey(String key);
}
