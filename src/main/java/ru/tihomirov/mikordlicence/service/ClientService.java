package ru.tihomirov.mikordlicence.service;

import ru.tihomirov.mikordlicence.domain.dto.LicenceCodeDto;
import ru.tihomirov.mikordlicence.domain.entity.Client;

public interface ClientService {

    LicenceCodeDto licenceCode(String key, String code);

    Client update(String key, String code);
}
