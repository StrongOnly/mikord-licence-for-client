package ru.tihomirov.mikordlicence.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.tihomirov.mikordlicence.service.CodeService;

import java.math.BigInteger;
import java.util.Collections;


@Service
public class CodeServiceImpl implements CodeService {

    private static final String EMPTY_STRING = "";
    private static final String DELIMITER = "-";
    private static final char DELIMITER_CHAR = '-';
    private static final String ZER0 = "0";

    @Value("${encoding.code.key}")
    private String key;

    public String encodeId(Long id) {
        BigInteger bigId = new BigInteger(id.toString(), 10);
        BigInteger bigKey = new BigInteger(key, 16);
        BigInteger result = bigId.xor(bigKey);

        String str = result.toString();

        str = String.join(EMPTY_STRING, Collections.nCopies(16 - str.length(), ZER0)) + str;

        StringBuilder sb = new StringBuilder(str);

        sb.insert(4, DELIMITER);
        sb.insert(9, DELIMITER);
        sb.insert(14, DELIMITER);

        return sb.toString();
    }

    public long decodeId(String encrypted, int key) {
        StringBuilder str = new StringBuilder();

        for (char c : encrypted.toCharArray()) {
            if (c != DELIMITER_CHAR) {
                str.append(c);
            }
        }

        return Long.parseLong(str.toString()) - key;
    }
}
