package ru.tihomirov.mikordlicence.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tihomirov.mikordlicence.domain.dto.LicenceCodeDto;
import ru.tihomirov.mikordlicence.domain.entity.Client;
import ru.tihomirov.mikordlicence.exception.NotFoundInDbException;
import ru.tihomirov.mikordlicence.repository.ClientRepository;
import ru.tihomirov.mikordlicence.service.ClientService;
import ru.tihomirov.mikordlicence.service.CodeService;
import ru.tihomirov.mikordlicence.service.validations.ClientValidationService;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientValidationService clientValidationService;

    private final ClientRepository clientRepository;

    private final CodeService codeService;

    @Override
    public LicenceCodeDto licenceCode(String key, String code) {
        clientValidationService.validateLicenseCode(key, code);
        int idInt = Integer.parseInt(key);
        long idLong = codeService.decodeId(code, idInt);
        code = codeService.encodeId(idLong);
        return LicenceCodeDto.of(code);
    }

    @Override
    public Client update(String key, String code) {
        clientValidationService.validateUpdate(key, code);
        Client client = findClientById(key);
        if (client.getFirstRequestDateTime() == null) {
            client.setFirstRequestDateTime(LocalDateTime.now());
        }
        int idInt = Integer.parseInt(key);
        client.setCodeAfterDecode(codeService.decodeId(code, idInt));
        int requestCount = client.getRequestCount();
        client.setRequestCount(++requestCount);
        return clientRepository.save(client);
    }

    private Client findClientById(String key) {
        return clientRepository.findByKey(key)
                .orElseThrow(() -> new NotFoundInDbException("Not found client with current id"));
    }
}
