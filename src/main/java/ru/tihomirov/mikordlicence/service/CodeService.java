package ru.tihomirov.mikordlicence.service;

public interface CodeService {

    String encodeId(Long id);

    long decodeId(String encrypted, int key);

}
