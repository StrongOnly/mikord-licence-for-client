package ru.tihomirov.mikordlicence.service.validations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tihomirov.mikordlicence.exception.NotFoundInDbException;
import ru.tihomirov.mikordlicence.exception.ValidationException;
import ru.tihomirov.mikordlicence.repository.ClientRepository;

@Service
@RequiredArgsConstructor
public class ClientValidationService {

    private static final String CODE_REGEX = "\\d{4}-\\d{4}-\\d{4}-\\d{4}";

    private final ClientRepository clientRepository;


    public void validateLicenseCode(String key, String code) {
        if (!clientRepository.existsByKey(key)) {
            throw new NotFoundInDbException("Not found client with current id");
        }
        if (!code.matches(CODE_REGEX)) {
            throw new ValidationException("Invalid code format");
        }
    }

    public void validateUpdate(String key, String code) {
        if (!code.matches(CODE_REGEX)) {
            throw new ValidationException("Invalid code format");
        }
    }
}
