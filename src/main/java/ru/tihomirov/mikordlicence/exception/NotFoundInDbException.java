package ru.tihomirov.mikordlicence.exception;

public class NotFoundInDbException extends RuntimeException {

    public NotFoundInDbException(String message) {
        super(message);
    }
}
