# Установка и настройка
## Создание БД
Выполните скрипт https://gitlab.com/StrongOnly/mikord-licence-for-client/-/blob/master/src/main/resources/dao/createDb.sql
## Запуск приложения
В настройках приложения сейчас задано:
путь до БД - jdbc:postgresql://localhost:5432/licenceClients2
логин к БД  - postgres
пароль к БД - postgres

Запускаем командой
```java -jar testspringboot-1.0-SNAPSHOT.jar```

Проверить можно, например, открыв в браузере ссылку http://localhost:8080/client?key=100001&code=1078-9195-6072-2303
где key - ключ клиента
code - код устройства
